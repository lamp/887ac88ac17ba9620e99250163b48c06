#!/bin/sh
/usr/local/bin/smc -k "FS! " -w 0007 # all forced mode
/usr/local/bin/smc -k "F0Tg" -w 1518 # ODD 1350 rpm
/usr/local/bin/smc -k "F1Tg" -w 1900 # HDD 1600 rpm
/usr/local/bin/smc -k "F2Tg" -w 12c0 # CPU 1200 rpm
